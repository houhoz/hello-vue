# FROM node:lts-alpine as build-stage
# WORKDIR /app
# COPY package*.json ./
# RUN npm install
# COPY . /app
# RUN npm run build

# # production stage
# FROM nginx:stable-alpine as production-stage
# COPY --from=build-stage /app/dist /usr/share/nginx/html

FROM node:lts-alpine AS builder

COPY package*.json /version/
RUN cd /version && npm version 0.0.0

FROM node:lts-alpine

WORKDIR /src
COPY --from=builder /version/package*.json ./
RUN npm install
COPY . /src
RUN npm run build

FROM nginx:stable-alpine

COPY --from=1 /src/dist /usr/share/nginx/html/


